#include "protocol.h"

using namespace std;

namespace hwo_protocol
{

  jsoncons::json make_request(const std::string& msg_type, const jsoncons::json& data)
  {
    jsoncons::json r;
    r["msgType"] = msg_type;
    r["data"] = data;
    return r;
  }

  jsoncons::json make_join(const std::string& name, const std::string& key)
  {
    jsoncons::json data;
    data["name"] = name;
    data["key"] = key;
    return make_request("join", data);
  }

  jsoncons::json make_create(const std::string& name, const std::string& key, const std::string& track_name, const std::string& password, const std::string& car_count)
  {
    jsoncons::json bot_id;
    bot_id["name"] = name;
    bot_id["key"] = key;
    jsoncons::json data;
    data["botId"] = bot_id;
    data["trackName"] = track_name;
    data["password"] = password;
    data["carCount"] = car_count;
    jsoncons::json msg = make_request("createRace", data);
    //cout << msg << end;
    return msg;
  }

  jsoncons::json make_ping()
  {
    return make_request("ping", jsoncons::null_type());
  }

  jsoncons::json make_throttle(double throttle)
  {
    return make_request("throttle", throttle);
  }

}  // namespace hwo_protocol
